package com.wop.main.ui.dashboard

import android.content.Context
import com.wop.main.data.model.Story
import com.wop.main.data.repository.DataProvider
import com.wop.main.ui.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import rx.Observable
import rx.Single
import rx.schedulers.Schedulers
import java.util.*

/**
 * Created by Vaibhav Yerawar on 6/12/2020.
 */
class DashboardPresenter : BasePresenter<DashboardFragment>(), DashboardContract.PresenterContract {

    override fun getStories() {
        dataProvider.getTopStoriesList()
                .subscribeOn(Schedulers.io())
                .doOnSuccess{storyIds-> view?.storiesIdList(storyIds)}
                .doOnError { error-> view?.showError(error.message?:"") }
    }

    override fun getStories(storyIds:List<String>) {
        val storyList = mutableListOf<Story>()
        Observable.from(storyIds)
                .subscribeOn(Schedulers.io())
                .onBackpressureBuffer()
                .doOnNext { storyId ->
                    dataProvider.getStory(storyId).
                            doOnSuccess{story-> storyList.add(story)}
                            .doOnError{error-> view?.showError(error.message.toString())}
                }
                .doOnCompleted { view?.topStoriesList(storyList) }
    }
}