package com.wop.main.ui.base

import android.support.v4.app.Fragment
import com.wop.main.data.model.Story


/**
 * Created by Vaibhav Yerawar on 6/18/2020.
 */
interface FragmentCallback {
    fun changeToNextFragment(fragment: Fragment, argStory: Story? = null)
    fun popBackOldFragment()
    fun showThemeChangerOption()
    fun hideThemeChangerOption()
    fun showProgressBar()
    fun hideProgressBar()
    fun showErrorAlert(errorMsg:String)
}