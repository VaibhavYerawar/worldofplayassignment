package com.wop.main.ui.login

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.wop.main.R
import com.wop.main.ui.base.BaseFragment
import com.wop.main.ui.dashboard.DashboardFragment
import com.wop.main.ui.dashboard.DashboardPresenter
import com.wop.main.utils.Utils
import io.reactivex.disposables.Disposable
import rx.Observable
import rx.Single
import java.util.concurrent.TimeUnit

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LoginFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : BaseFragment(), LoginContract.ViewContract {

    private lateinit var edtTxtEmail: TextInputEditText
    private lateinit var edtTxtPassword: TextInputEditText
    private lateinit var btnDoLogin: Button
    private lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layoutView = inflater.inflate(R.layout.fragment_login_layout, null)
        initUIComponents(layoutView)
        bindListeners()
        return layoutView
    }

    private fun init(){
        presenter = LoginPresenter()
    }

    private fun initUIComponents(layoutView: View) {
        edtTxtEmail  = layoutView.findViewById(R.id.edtTxtEmail)
        edtTxtPassword  = layoutView.findViewById(R.id.edtTxtPassword)
        btnDoLogin = layoutView.findViewById(R.id.btnDoLogin)
    }

    private fun bindListeners(){
        btnDoLogin.setOnClickListener {loginBtn->
            fragmentCallback.showProgressBar()
            presenter.doLogin(edtTxtEmail.text.toString(), edtTxtPassword.text.toString())
        }

        edtTxtPassword.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(typedText: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        Observable.just(typedText)
                                .debounce(600, TimeUnit.MILLISECONDS)
                                .map { text -> Utils.isValidPasswordStrength(text.toString()) }
                                .subscribe { isValid ->
                                    if (isValid)
                                        edtTxtPassword.error = "Password is too week."
                                    else
                                        edtTxtPassword.error = ""
                                }

            }
        })
    }

    override fun onLoginSuccessful() {
        fragmentCallback.hideProgressBar()
        fragmentCallback.changeToNextFragment(DashboardFragment.getInstance())
    }

    override fun onLoginFailed(errorMsg: String) {
        fragmentCallback.showErrorAlert(errorMsg)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
        presenter.setViewContext(context)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }
}