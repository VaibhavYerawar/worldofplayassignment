package com.wop.main.ui.login

/**
 * Created by Vaibhav Yerawar on 6/12/2020.
 */
interface LoginContract {
    interface ViewContract {
        fun onLoginSuccessful()
        fun onLoginFailed(errorMsg:String="")
    }

    interface PresenterContract {
        fun doLogin(userName:String, password:String)
    }
}