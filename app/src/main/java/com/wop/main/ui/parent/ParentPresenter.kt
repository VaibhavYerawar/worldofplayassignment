package com.wop.main.ui.parent

import com.wop.main.data.repository.DataProvider

/**
 * Created by Vaibhav Yerawar on 6/18/2020.
 */
class ParentPresenter(val parentActivity: ParentActivity): ParentContract.PresenterContract {
    private val dataProvider = DataProvider(parentActivity)

    override fun saveThemeCode(themeCode: Int) {
        dataProvider.saveThemeToStorage(themeCode)
    }

    fun getThemeCode(){
        parentActivity.applyTheme(dataProvider.getThemeFromStorage())
    }
}