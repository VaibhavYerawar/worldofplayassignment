package com.wop.main.ui.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.wop.main.R
import com.wop.main.data.model.Story

/**
 * Created by Vaibhav Yerawar on 6/15/2020.
 */
class StoryAdapter(val listener:(Story) -> Unit) : RecyclerView.Adapter<StoryAdapter.CellViewHolder>() {

    private val storyList = mutableListOf<Story>()

    override fun getItemCount(): Int {
        return storyList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CellViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellLayoutView = layoutInflater.inflate(R.layout.story_list_cell_layout, null)
        return CellViewHolder(cellLayoutView)
    }

    override fun onBindViewHolder(holder: CellViewHolder, position: Int) {
        val story = storyList[position]
        holder.txtViewCellTitle?.text = story.title
        holder.txtViewCellType?.text = story.type
        holder.storyItemCard?.setOnClickListener{listener(story)}
    }

    class CellViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtViewCellTitle = view.findViewById<TextView>(R.id.txtViewCellTitle)
        val txtViewCellType = view.findViewById<TextView>(R.id.txtViewCellType)
        val storyItemCard = view.findViewById<CardView>(R.id.storyItemCard)
    }

    fun addStories(storyList: List<Story>) {
        val oldLimit = this.storyList.size
        this.storyList.addAll(storyList)
        notifyItemRangeInserted(oldLimit, storyList.size)
    }

    fun clearAll() {
        this.storyList.clear()
        notifyDataSetChanged()
    }
}