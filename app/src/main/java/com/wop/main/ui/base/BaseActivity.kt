package com.wop.main.ui.base

import android.app.Dialog
import android.content.DialogInterface
import android.content.DialogInterface.*
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar
import com.wop.main.R
import com.wop.main.data.model.Story

/**
 * Created by Vaibhav Yerawar on 6/12/2020.
 */
abstract class BaseActivity : AppCompatActivity(), FragmentCallback {

    private var mProgressDialog : Dialog? = null
    private lateinit var alertDialog : AlertDialog
    private val fragmentTransaction = this.supportFragmentManager.beginTransaction()
    private lateinit var argStory:Story
    protected val argKey = getString(R.string.key_story_frag_arg)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initProgressDialog()
        initAlertDialog()
        hideAction()
    }

    protected fun hideAction() {
        supportActionBar!!.hide()
    }

    protected fun setDarkTheme() {
        setTheme(android.R.style.Theme_Black)
        this.recreate()
    }

    protected fun setLigthTheme() {
        setTheme(android.R.style.Theme_Light)
        this.recreate()
    }

    private fun initProgressDialog() {
        if (mProgressDialog == null) {
            val progressBar = ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal)
            progressBar.isIndeterminate = true
            progressBar.max = 100
            progressBar.visibility = View.GONE
            mProgressDialog = Dialog(this)
            mProgressDialog?.setContentView(progressBar)
            mProgressDialog?.setCancelable(false)
        }
    }

    protected fun showProgress() {
        if (mProgressDialog?.isShowing == false) {
            mProgressDialog?.show()
        }
    }

    protected fun hideProgress() {
        if (mProgressDialog?.isShowing == true) {
            mProgressDialog?.hide()
        }
    }

    private fun initAlertDialog() {
        alertDialog = AlertDialog.Builder(this)
                .setPositiveButton("OK") { dialog, which -> alertDialog.dismiss() }
                .setTitle("Alert")
                .setMessage("")
                .create()
    }

    protected fun showAlert(alterMsg:String){
        alertDialog.setMessage(alterMsg)
        alertDialog.show()
    }

    protected fun addFragment(containerViewId:Int, fragment: Fragment){
        fragmentTransaction.add(containerViewId, fragment)
        fragmentTransaction.commit()
    }

    protected fun replaceFragment(containerViewId:Int, fragment: Fragment, pushToStack:Boolean = false){
        fragmentTransaction.replace(containerViewId, fragment)
        if(pushToStack) {
            fragmentTransaction.addToBackStack(null)
        }
        fragmentTransaction.commit()
    }

    protected fun popBackFragment(){
        this.supportFragmentManager.popBackStackImmediate()
    }
}