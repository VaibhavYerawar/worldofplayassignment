package com.wop.main.ui.dashboard

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.wop.main.R
import com.wop.main.data.model.Story
import com.wop.main.ui.adapter.StoryAdapter
import com.wop.main.ui.adapter.StoryItemClickListener
import com.wop.main.ui.base.BaseFragment
import com.wop.main.ui.base.FragmentCallback
import com.wop.main.ui.detail.DetailFragment

/**
 * Created by Vaibhav Yerawar on 6/12/2020.
 */
class DashboardFragment : BaseFragment(), DashboardContract.ViewContract, StoryItemClickListener {

    private lateinit var recycleStoryLst: RecyclerView
    private lateinit var storyListAdapter: StoryAdapter
    private lateinit var presenter: DashboardPresenter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var storyLstBtmProgressBar: ProgressBar
    private lateinit var storyIds: List<String>
    private var isNotLoading = true

    companion object {
        fun getInstance() = DashboardFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layoutView = inflater.inflate(R.layout.fragment_dashboard_layout, null)
        initUIComponents(layoutView)
        bindListeners()
        return layoutView
    }

    private fun init() {
        presenter = DashboardPresenter()
        storyListAdapter = StoryAdapter(){story ->

        }
        layoutManager = LinearLayoutManager(this.activity)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
    }

    private fun initUIComponents(layoutView: View) {
        recycleStoryLst = layoutView.findViewById(R.id.recycleStoryLst)
        recycleStoryLst.itemAnimator = DefaultItemAnimator()
        swipeRefreshLayout = layoutView.findViewById(R.id.swipeRefreshLayout)
        storyLstBtmProgressBar = layoutView.findViewById(R.id.storyLstBtmProgressBar)
        recycleStoryLst.layoutManager = layoutManager
    }

    private fun bindListeners() {
        swipeRefreshLayout.setOnRefreshListener(this::refreshStoryList)

        recycleStoryLst.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val lastItemPos = layoutManager.childCount.minus(1)
                    val lastVisibleItem = layoutManager.findLastCompletelyVisibleItemPosition()
                    if (lastItemPos == lastVisibleItem) {
                        storyLstBtmProgressBar.visibility = View.VISIBLE
                        loadMoreStories(lastItemPos)
                    }
                }
            }
        })

        swipeRefreshLayout.isRefreshing = true
    }

    override fun topStoriesList(storyLst: List<Story>) {
        storyLstBtmProgressBar.visibility = View.GONE
        storyListAdapter.addStories(storyLst)
        isNotLoading = true
    }

    override fun storiesIdList(storyIdLst: List<String>) {
        storyIds = storyIdLst
    }

    override fun showError(errorMsg: String) {
        storyLstBtmProgressBar.visibility = View.GONE
        fragmentCallback.showErrorAlert(errorMsg)
    }

    private fun refreshStoryList() {
        if (storyIds.isNotEmpty() && isNotLoading) {
            isNotLoading = false
            storyListAdapter.clearAll()
            presenter.getStories(storyIds.subList(0, 20))
        }
    }

    private fun loadMoreStories(offset: Int = 0) {
        if (storyIds.isNotEmpty() && isNotLoading) {
            isNotLoading = false
            presenter.getStories(storyIds.subList(offset, offset.plus(20)))
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attachView(this)
        presenter.setViewContext(context)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detachView()
    }

    override fun onStoryItemClick(story: Story) {
        fragmentCallback.changeToNextFragment(DetailFragment.getInstance(), story)
    }
}