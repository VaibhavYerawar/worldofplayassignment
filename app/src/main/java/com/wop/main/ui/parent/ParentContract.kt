package com.wop.main.ui.parent

/**
 * Created by Vaibhav Yerawar on 6/19/2020.
 */
interface ParentContract {

    interface ViewContract {
        fun applyTheme(themeCode: Int)
    }

    interface PresenterContract {
        fun saveThemeCode(themeCode:Int)
    }
}