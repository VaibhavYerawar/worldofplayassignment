package com.wop.main.ui.adapter

import com.wop.main.data.model.Story

interface StoryItemClickListener {
    fun onStoryItemClick(story: Story)
}