package com.wop.main.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import com.wop.main.R
import com.wop.main.data.model.Story
import com.wop.main.ui.base.BaseFragment

/**
 * Created by Vaibhav Yerawar on 6/12/2020.
 */
class DetailFragment : BaseFragment(){

    private lateinit var txtViewTitle:TextView
    private lateinit var webViewStoreDetail:WebView

    companion object {
        fun getInstance() = DetailFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layoutView = inflater.inflate(R.layout.fragment_details_layout, null)
        initUIComponents(layoutView)
        return layoutView
    }

    override fun onStart() {
        super.onStart()
        val story:Story = this.arguments?.get(story_obj_key) as Story
        showStoryDetails(story)
    }

    private fun showStoryDetails(story: Story){
        txtViewTitle.text = story.title
        webViewStoreDetail.loadUrl(story.url)
    }

    private fun initUIComponents(layoutView:View){
        txtViewTitle = layoutView.findViewById(R.id.txtViewTitle)
        webViewStoreDetail = layoutView.findViewById(R.id.webViewStoreDetail)
        webViewStoreDetail.settings.javaScriptEnabled = true
        webViewStoreDetail.webViewClient = object:WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                view?.loadUrl(request?.url.toString())
                return true
            }
        }
    }
}