package com.wop.main.ui.base

import android.content.Context
import com.wop.main.data.repository.DataProvider

abstract class BasePresenter<V> {

    protected var view: V? = null
    protected var context: Context? = null
    protected val dataProvider = DataProvider(context)

    fun attachView(view: V) {
        this.view = view
    }

    fun detachView() {
        this.view = null
    }

    fun setViewContext(context: Context?) {
        this.context = context
    }
}