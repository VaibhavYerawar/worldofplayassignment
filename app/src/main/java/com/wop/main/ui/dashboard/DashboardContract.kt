package com.wop.main.ui.dashboard

import com.wop.main.data.model.Story

/**
 * Created by Vaibhav Yerawar on 6/12/2020.
 */
interface DashboardContract {

    interface ViewContract {
        fun storiesIdList(storyIdLst:List<String>)
        fun topStoriesList(storyLst:List<Story>)
        fun showError(errorMsg:String = "")
    }

    interface PresenterContract {
        fun getStories()
        fun getStories(storyIds:List<String>)

    }
}