package com.wop.main.ui.parent


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.widget.Switch
import com.wop.main.R
import com.wop.main.data.model.Story
import com.wop.main.ui.base.BaseActivity
import com.wop.main.ui.base.BaseFragment
import com.wop.main.ui.base.FragmentCallback
import com.wop.main.ui.dashboard.DashboardFragment
import com.wop.main.ui.detail.DetailFragment
import com.wop.main.ui.login.LoginFragment

class ParentActivity : BaseActivity(), ParentContract.ViewContract {

    private lateinit var swtTheme:Switch
    private lateinit var presenter: ParentPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.parent_activity_layout)
        init()
        bindListeners()
        presenter.getThemeCode()
        addFragment(R.id.childFragmentContainer, LoginFragment())
    }

    private fun init(){
        swtTheme = findViewById(R.id.swtTheme)
        presenter = ParentPresenter(this)
    }

    private fun bindListeners() {
        swtTheme.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setLigthTheme()
            } else {
                setDarkTheme()
            }
        }
    }

    override fun changeToNextFragment(fragment: Fragment, argStory: Story?) {
        fragment.arguments?.putParcelable(argKey, argStory)
        replaceFragment(R.id.childFragmentContainer, fragment)
    }

    override fun applyTheme(themeCode: Int) {
        when (themeCode) {
             0 -> setLigthTheme()
             1 -> setDarkTheme()
        }
    }

    override fun showThemeChangerOption() {
        swtTheme.visibility = View.VISIBLE
    }

    override fun hideThemeChangerOption() {
        swtTheme.visibility = View.GONE
    }

    override fun popBackOldFragment() {
        popBackFragment()
    }

    override fun showErrorAlert(errorMsg: String) {
        showAlert(errorMsg)
    }

    override fun showProgressBar() {
        TODO("Not yet implemented")
    }

    override fun hideProgressBar() {
        TODO("Not yet implemented")
    }

    override fun onAttachFragment(fragment: Fragment?) {
        super.onAttachFragment(fragment)
        (fragment as BaseFragment).registerFragmentCallbackListner(this)
    }
}
