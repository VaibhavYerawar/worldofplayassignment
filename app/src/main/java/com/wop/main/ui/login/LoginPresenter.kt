package com.wop.main.ui.login

import android.content.Context
import com.wop.main.data.model.LoginRequest
import com.wop.main.data.repository.DataProvider
import com.wop.main.ui.base.BasePresenter
import com.wop.main.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


/**
 * Created by Vaibhav Yerawar on 6/12/2020.
 */
class LoginPresenter: BasePresenter<LoginFragment>(), LoginContract.PresenterContract {

    override fun doLogin(userName: String, password: String) {
        if(Utils.isValidUserName(userName)) {
            val loginRequest = LoginRequest(userName,password)
            dataProvider.loginSession(loginRequest)
                    .subscribeOn(Schedulers.io())
                    //.observeOn(AndroidSchedulers.mainThread())
                    .doOnCompleted{view?.onLoginSuccessful()}
                    .doOnError{ loginError->
                        view?.onLoginFailed(loginError.message.toString())
                    }
        }else{
            view?.onLoginFailed("Invalid username or password.")
        }
    }
}