package com.wop.main.ui.base

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import com.wop.main.R
import com.wop.main.ui.parent.ParentActivity


/**
 * Created by Vaibhav Yerawar on 6/12/2020.
 */
abstract class BaseFragment : Fragment() {

    protected lateinit var fragmentCallback: FragmentCallback
    protected val story_obj_key = context?.getString(R.string.key_story_frag_arg)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun registerFragmentCallbackListner(fragmentCallback: FragmentCallback) {
        this.fragmentCallback = fragmentCallback
    }
}