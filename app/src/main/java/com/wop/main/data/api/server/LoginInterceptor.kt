package com.wop.main.data.api.server

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.net.ConnectivityManager
import android.net.Uri
import com.google.gson.Gson
import com.wop.main.data.api.NetworkManager
import okhttp3.*
import java.util.*

/**
 * Created by Vaibhav Yerawar on 6/17/2020.
 */
class LoginInterceptor(context: Context?): Interceptor {
    val gsonClient = Gson()
//    val connectivityManager:ConnectivityManager? = context?.getSystemService(Service.CONNECTIVITY_SERVICE) as ConnectivityManager

    override fun intercept(chain: Interceptor.Chain?): Response? {

        val uri = Uri.parse(chain?.request()?.url().toString())

        if (uri.path !== "/login") {
            return chain?.proceed(chain.request())
        }

        val requestBody = chain?.request()?.body()
        var responseCode = 0
        var loginResponseStr = ""

        if (verifyNetworkConnectivity()) {
            responseCode = 400
            loginResponseStr = gsonClient.toJson(LoginServerResponse(errorMsg = "Invalid Credentials",
                    description = "Network Communication Error."))
        } else if (authenticate(requestBody.toString())) {
            val token = generateNewToken()
            responseCode = 200
            loginResponseStr = gsonClient.toJson(LoginServerResponse(token))
        } else {
            responseCode = 401
            loginResponseStr = gsonClient.toJson(LoginServerResponse(errorMsg = "Invalid Credentials",
                    description = "Invalid email or password."))
        }

        val responseBody = ResponseBody.create(MediaType.parse("application/json"), loginResponseStr)
        return Response.Builder()
                .code(responseCode)
                .message(loginResponseStr)
                .protocol(Protocol.HTTP_1_0)
                .request(chain?.request())
                .body(responseBody)
                .addHeader("content-type", "application/json")
                .build()
    }

    private fun authenticate(request: String): Boolean {
        val savedUserCredentials = UserCredentials()
        val requestObj = gsonClient.fromJson(request, LoginServerRequest::class.java)
        val isValidUserName = requestObj.userName.hashCode() == savedUserCredentials.userNameHashCode
        val isValidPassword = requestObj.password.hashCode() == savedUserCredentials.passwordHashCode
        return isValidUserName && isValidPassword
    }

    @SuppressLint("NewApi")
    private fun generateNewToken(): String {
        val source: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
        return Random().ints(10, 0, source.size)
                .iterator()
                .asSequence()
                .map(source::get)
                .joinToString("")
    }

    private data class UserCredentials(private val userName: String = "test@worldofplay.in",
                                       private val password: String = "Worldofplay@2020") {
        val userNameHashCode: Int
            get() = userName.hashCode()

        val passwordHashCode: Int
            get() = password.hashCode()
    }

    private fun verifyNetworkConnectivity() = true
           // connectivityManager?.isDefaultNetworkActive == true && connectivityManager.activeNetworkInfo?.isConnected == true
}