package com.wop.main.data.repository

import android.content.Context
import com.google.gson.Gson
import com.wop.main.data.api.NetworkManager
import com.wop.main.data.model.LoginRequest
import com.wop.main.data.model.Story
import com.wop.main.utils.Utils
import rx.Completable
import rx.Single
import rx.schedulers.Schedulers
import java.util.*

/**
 * Created by Vaibhav Yerawar on 6/13/2020.
 */
class DataProvider(context:Context?) {

    private val networkManager = NetworkManager(context)
    private val cacheManager = CacheManager(context)
    private val isSessionExpire = isStorageEmptyExpired()

    fun loginSession(loginRequest: LoginRequest): Completable {
        val key = "sessionToken"
        val sessionToken = cacheManager.readFromCache(key)
        return Completable.create { emitter ->
            if (isSessionExpire || sessionToken.isNullOrEmpty()) {
                networkManager.getApi().doLogin(loginRequest)
                        .subscribeOn(Schedulers.io())
                        .doOnSuccess { loginResponse ->
                            cacheManager.writeToCache(key, loginResponse.sessionToken)
                            emitter.onCompleted()
                        }
                        .doOnError {loginError-> emitter.onError(loginError) }
            } else {
                emitter.onCompleted()
            }
        }
    }

    fun getTopStoriesList(): Single<List<String>> {
        val key = "storyList"
        val storyIdList = cacheManager.readStringSetFromCache(key)?.toList()
        return Single.create { emitter ->
            if (isSessionExpire || storyIdList?.isEmpty()==true) {
                networkManager.getApi().getTopStories()
                        .subscribeOn(Schedulers.io())
                        .doOnSuccess { idsList ->
                            cacheManager.writeToCache(key, idsList.toSet())
                            emitter.onSuccess(idsList)
                        }
                        .doOnError { emitter.onError(RuntimeException("No data found.")) }
            } else {
                emitter.onSuccess(storyIdList)
            }
        }
    }

    fun getStory(storyId: String): Single<Story> {
        val story = cacheManager.readFromCache(storyId)
        return Single.create { emitter ->
            if (isSessionExpire || story.isNullOrEmpty()) {
                networkManager.getApi().getStoryDetails(storyId)
                        .subscribeOn(Schedulers.io())
                        .doOnSuccess { storyJson ->
                            cacheManager.writeToCache(storyId, storyJson)
                            emitter.onSuccess(Gson().fromJson(storyJson, Story::class.java))
                        }
                        .doOnError { emitter.onError(RuntimeException("No data found.")) }
            } else {
                emitter.onSuccess(Gson().fromJson(story, Story::class.java))
            }
        }
    }

    fun saveThemeToStorage(themeCode:Int){
        cacheManager.writeToCache("themeCode", themeCode)
    }

    fun getThemeFromStorage():Int{
       return cacheManager.readIntFromCache("themeCode")?:0
    }

    private fun isAvailableNotInStorage(key: String) =
            cacheManager.isAvailableInCache(key)?:true

    private fun isStorageEmptyExpired(): Boolean {
        val sdf = Utils.dateFormater()
        val today: Date = Utils.todayDate()
        val expireDateStr = cacheManager.readFromCache("expireDate")
        if (expireDateStr?.isNotEmpty()== true) {
            val expireDate = sdf.parse(expireDateStr)
            if (today.before(expireDate)) {
                return false
            }
        }
        cacheManager.writeToCache("expireDate", Utils.dateAfterDays(15))
        return true
    }
}