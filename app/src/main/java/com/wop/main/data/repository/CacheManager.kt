package com.wop.main.data.repository

import android.content.Context
import android.content.SharedPreferences
import android.preference.Preference

/**
 * Created by Vaibhav Yerawar on 6/13/2020.
 */
class CacheManager(context:Context?) {

    private val STORAGE_NAME = "LOCAL_STORAGE"
    private val sharedPrefInstance = context?.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE)

    fun writeToCache(key: String, value: String) {
        sharedPrefInstance?.edit()
                ?.putString(key, value)
                ?.apply()
    }

    fun writeToCache(key: String, value: Int) {
        sharedPrefInstance?.edit()
                ?.putInt(key, value)
                ?.apply()
    }

    fun writeToCache(key: String, value: Set<String>) {
        sharedPrefInstance?.edit()
                ?.putStringSet(key, value)
                ?.apply()
    }

    fun readFromCache(key: String) =
            sharedPrefInstance?.getString(key, "")

    fun readIntFromCache(key: String) =
            sharedPrefInstance?.getInt(key, 0)

    fun readStringSetFromCache(key: String) =
            sharedPrefInstance?.getStringSet(key, emptySet<String>())

    fun deleteFromCache(key: String) = sharedPrefInstance?.edit()
            ?.remove(key)
            ?.apply()

    fun clearCache() = sharedPrefInstance?.edit()
            ?.clear()
            ?.apply()

    fun isAvailableInCache(key:String) =
            sharedPrefInstance?.contains(key)
}