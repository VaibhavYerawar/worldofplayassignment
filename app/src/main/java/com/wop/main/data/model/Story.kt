package com.wop.main.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by Vaibhav Yerawar on 6/13/2020.
 */
data class Story (
    @SerializedName("id")
    var id: String,

    @SerializedName("score")
    var score: String,

    @SerializedName("title")
    var title: String,

    @SerializedName("type")
    var type: String,

    @SerializedName("url")
    var url: String ):Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeString(score)
        dest?.writeString(title)
        dest?.writeString(type)
        dest?.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Story> {
        override fun createFromParcel(parcel: Parcel): Story {
            return Story(parcel)
        }

        override fun newArray(size: Int): Array<Story?> {
            return arrayOfNulls(size)
        }
    }
}