package com.wop.main.data.api

import android.content.Context
import com.wop.main.data.api.server.LoginInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Vaibhav Yerawar on 6/13/2020.
 */
class NetworkManager(val context: Context?) {

    private val baseURL = "https://hacker-news.firebaseio.com/"
    private lateinit var apiInstance: Api

    init {
        initNetworkAdapter()
    }

    private fun initNetworkAdapter() {
        val httpClient = OkHttpClient()
        httpClient.interceptors().add(LoginInterceptor(context))
        apiInstance = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(baseURL)
                .client(httpClient)
                .build().create(Api::class.java)
    }

    fun getApi() = apiInstance
}