package com.wop.main.data.api

import com.wop.main.data.model.LoginRequest
import com.wop.main.data.model.LoginResponse
import retrofit2.http.*
import rx.Single

/**
 * Created by Vaibhav Yerawar on 6/12/2020.
 */
interface Api {

    @POST("/login")
    fun doLogin(loginRequest: LoginRequest): Single<LoginResponse>

    @GET("v0/topstories.json")
    fun getTopStories(): Single<List<String>>

    @GET("v0/item/{story_Id}.json")
    fun getStoryDetails(@Path(value = "story_Id", encoded = true) storyId: String): Single<String>
}