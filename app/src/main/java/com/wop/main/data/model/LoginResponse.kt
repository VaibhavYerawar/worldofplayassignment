package com.wop.main.data.model

import java.io.Serializable

/**
 * Created by Vaibhav Yerawar on 6/15/2020.
 */
data class LoginResponse(val sessionToken:String, val description:String)