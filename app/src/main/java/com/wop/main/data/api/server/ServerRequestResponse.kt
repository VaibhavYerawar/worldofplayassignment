package com.wop.main.data.api.server

/**
 * Created by Vaibhav Yerawar on 6/15/2020.
 */
data class LoginServerRequest(val userName:String, val password:String)
data class LoginServerResponse(val sessionToken:String="",val errorMsg:String="", val description:String="")