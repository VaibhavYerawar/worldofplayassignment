package com.wop.main.utils

import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * Created by Vaibhav Yerawar on 6/14/2020.
 */
class Utils {

    companion object {
        fun patternValidator(input: String, regEx: String) =
                Pattern.matches(regEx, input)

        fun dateFormater() =
                SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)

        fun todayDate() =
                Calendar.getInstance().time

        fun dateAfterDays(days:Int = 0) : String {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DATE, days)
         return dateFormater().format(calendar.time)
        }

        fun isValidPasswordStrength(password:String): Boolean {
            val regExPasswordValidate = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,16}$"
            return patternValidator(regExPasswordValidate, password)
        }

        fun isValidUserName(userName:String): Boolean {
            val regExEmailValidate = "/^(([^<>()\\[\\]\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/"
            return patternValidator(regExEmailValidate, userName)
        }
    }
}