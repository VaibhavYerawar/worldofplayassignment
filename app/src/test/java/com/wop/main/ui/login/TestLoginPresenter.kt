package com.wop.main.ui.login

import com.wop.main.data.api.NetworkManager
import com.wop.main.data.api.server.LoginInterceptor
import com.wop.main.data.model.LoginRequestBuilder
import com.wop.main.data.repository.DataProvider
import com.wop.main.ui.base.BasePresenter
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.powermock.core.classloader.annotations.PrepareForTest
import rx.Completable

@PrepareForTest(NetworkManager::class, LoginInterceptor::class, BasePresenter::class, LoginFragment::class,DataProvider::class)
@RunWith(MockitoJUnitRunner::class)
class TestLoginPresenter: LoginContract.ViewContract {

    private val presenter = LoginPresenter()
    @Mock
    private lateinit var loginFragment: LoginFragment //= mock(LoginFragment::class.java)
    @Mock
    private lateinit var dataProvider: DataProvider //= mock(DataProvider::class.java)
    private var testCaseType: Int = 0
    private lateinit var loginRequestBuilder: LoginRequestBuilder

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        presenter.attachView(loginFragment)
        loginRequestBuilder = LoginRequestBuilder()
    }

    @Test
    fun testLoginPositive() {
        val loginRequest = loginRequestBuilder
                .setExpectedUserName()
                .setExpectedPassword()
                .build()
        `when`(dataProvider.loginSession(any())).thenReturn(Completable.complete())
        presenter.doLogin(loginRequest.userName, loginRequest.password)
    }

    @Test
    fun testLoginNegativeIncorrectUserName() {
        testCaseType = 1
        val loginRequest = loginRequestBuilder
                .setIncorrectUserName()
                .setExpectedPassword()
                .build()
        `when`(dataProvider.loginSession(any())).thenReturn(Completable.complete())
        presenter.doLogin(loginRequest.userName, loginRequest.password)
    }

    @Test
    fun testLoginNegativeIncorrectPassword() {
        testCaseType = 1
        val loginRequest = loginRequestBuilder
                .setExpectedUserName()
                .setIncorrectPassword()
                .build()
        `when`(dataProvider.loginSession(any())).thenReturn(Completable.complete()) //error { t-> })
        presenter.doLogin(loginRequest.userName, loginRequest.password)
    }

    override fun onLoginSuccessful() {
        Assert.assertTrue("Login successful.", true)
    }

    override fun onLoginFailed(errorMsg: String) {
        when (testCaseType) {
            0 -> {
                Assert.assertEquals(errorMsg, "Invalid email or password.")
            }
            else -> {
                Assert.assertEquals(errorMsg, "Invalid Credentials")
            }
        }
    }
}
