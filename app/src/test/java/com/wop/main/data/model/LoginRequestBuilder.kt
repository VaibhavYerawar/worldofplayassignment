package com.wop.main.data.model

class LoginRequestBuilder {

    private lateinit var loginRequest: LoginRequest
    private lateinit var userName:String
    private lateinit var password:String

    fun setExpectedUserName():LoginRequestBuilder{
        userName = "test@worldofplay.in"
        return this
    }

    fun setExpectedPassword():LoginRequestBuilder{
        password = "Worldofplay@2020"
        return this
    }

    fun setIncorrectUserName():LoginRequestBuilder{
        userName = "test@worldofplay"
        return this
    }

    fun setIncorrectPassword():LoginRequestBuilder{
        password = "worldofplay"
        return this
    }

    fun build():LoginRequest = LoginRequest(userName, password)
}